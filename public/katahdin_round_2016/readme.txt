Katahdin Round


Enjoy this typeface! The license document has more legal information but I'd like to break it down as simply as possible here.

// CAN do with this:
Get paid for great designs using this font! Embed it in your iphone/ipad/android/mobile application. Create logos/brands for yourself or clients with this font. 

// CAN’T do with this:
Sell it and/or redistribute it. That's about it. That includes using it as a webfoot (unless you base64 encode it, in which case you’re good to go).

// SHOULD do with this:
Use it! The font was optimized for headline/display type and it will look incredibly crisp and beautiful on print and web products. For web use, the bigger the better.

// If you have questions on how to stack it, send me a message. Each style is numbered in the order it SHOULD be used IF layering styles.   


// ENJOY:  Write hi@tylerfinck.com if you want to show me. Or tweet at @finck, Instagram at @typeler


***

I plan on continuing to refine the font and you will receive updated files as they become available. Thank you again for your support. 

Tyler Finck
